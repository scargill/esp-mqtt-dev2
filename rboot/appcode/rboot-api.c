//////////////////////////////////////////////////
// rBoot OTA and config API for ESP8266.
// Copyright 2015 Richard A Burton
// richardaburton@gmail.com
// See license.txt for license terms.
// OTA code based on SDK sample from Espressif.
//////////////////////////////////////////////////

#include <string.h>
#include <c_types.h>
#include <spi_flash.h>
#include "rboot-api.h"

// this page doctored - Could not get hold of Richard Burton (RBOOT) when SDK 2.1.0 started causing memory errors
// turns out that the MALLOCS were occasionally too high and RBOOT was not handling it.
// This fellow helped.. https://github.com/piersfinlayson/otb-iot/tree/master/lib/rboot
// His code - with some doctoring - I used to update this file to only allow smaller MALLOCS
// it appears to be 100%


#define os_free(s)        vPortFree(s, "", 0)
#define os_malloc(s)      pvPortMalloc(s, "", 0)
#define os_calloc(s)      pvPortCalloc(s, "", 0);
#define os_realloc(p, s)  pvPortRealloc(p, s, "", 0)
#define os_zalloc(s)      pvPortZalloc(s, "", 0)

// detect rtos sdk (not ideal method!)
//#ifdef IRAM_ATTR
//#define os_free(s)   vPortFree(s)
//#define os_malloc(s) pvPortMalloc(s)
//#else
#include <mem.h>
//#endif


#ifdef RBOOT_INTEGRATION
#include <rboot-integration.h>
#endif

#include "rboot-api.h"

#ifdef __cplusplus
extern "C" {
#endif

extern void ets_printf(char*, ...);

#if defined(BOOT_CONFIG_CHKSUM) || defined(BOOT_RTC_ENABLED)
// calculate checksum for block of data
// from start up to (but excluding) end
static uint8 calc_chksum(uint8 *start, uint8 *end) {
	uint8 chksum = CHKSUM_INIT;
	while(start < end) {
		chksum ^= *start;
		start++;
	}
	return chksum;
}
#endif

// get the rboot config
rboot_config ICACHE_FLASH_ATTR rboot_get_config(void) {
	rboot_config conf;
	spi_flash_read(BOOT_CONFIG_SECTOR * SECTOR_SIZE, (uint32*)&conf, sizeof(rboot_config));
	return conf;
}

// write the rboot config
// preserves the contents of the rest of the sector,
// so the rest of the sector can be used to store user data
// updates checksum automatically (if enabled)
bool ICACHE_FLASH_ATTR rboot_set_config(rboot_config *conf) {
	uint8 *buffer;
	buffer = (uint8*)os_malloc(SECTOR_SIZE);
	if (!buffer) {
		//os_printf("No ram!\r\n");
		return false;
	}
	
#ifdef BOOT_CONFIG_CHKSUM
	conf->chksum = calc_chksum((uint8*)conf, (uint8*)&conf->chksum);
#endif
	
	spi_flash_read(BOOT_CONFIG_SECTOR * SECTOR_SIZE, (uint32*)((void*)buffer), SECTOR_SIZE);
	memcpy(buffer, conf, sizeof(rboot_config));
	spi_flash_erase_sector(BOOT_CONFIG_SECTOR);
	//spi_flash_write(BOOT_CONFIG_SECTOR * SECTOR_SIZE, (uint32*)((void*)buffer), SECTOR_SIZE);
	spi_flash_write(BOOT_CONFIG_SECTOR * SECTOR_SIZE, (uint32*)((void*)buffer), SECTOR_SIZE);
	
	os_free(buffer);
	return true;
}

// get current boot rom
uint8 ICACHE_FLASH_ATTR rboot_get_current_rom(void) {
	rboot_config conf;
	conf = rboot_get_config();
	return conf.current_rom;
}

// set current boot rom
bool ICACHE_FLASH_ATTR rboot_set_current_rom(uint8 rom) {
	rboot_config conf;
	conf = rboot_get_config();
	if (rom >= conf.count) return false;
	conf.current_rom = rom;
	return rboot_set_config(&conf);
}

// create the write status struct, based on supplied start address
rboot_write_status ICACHE_FLASH_ATTR rboot_write_init(uint32 start_addr) {
	rboot_write_status status = {0};
	status.start_addr = start_addr;
	status.start_sector = start_addr / SECTOR_SIZE;
	status.last_sector_erased = status.start_sector - 1;
	//status.max_sector_count = 200;
	//os_printf("init addr: 0x%08x\r\n", start_addr);
	return status;
}

#define RBOOT_OTA_MAX_LEN 1460

// function to do the actual writing to flash
// call repeatedly with more data (max len per write is the flash sector size (4k))
bool ICACHE_FLASH_ATTR rboot_write_flash(rboot_write_status *status, uint8 *data, uint16 len) {
	
	bool ret = false;
	uint8 *buffer = NULL;
	uint16 buf_len;
 	uint16 llen;
 	uint16 rlen;
 	uint8 *ldata;

	int32 lastsect;
	
		if (data == NULL || len == 0)
	 	{
	 		ret = true;
	 		goto EXIT_LABEL;
	}


	// get a buffer
		buf_len = len >= RBOOT_OTA_MAX_LEN ? RBOOT_OTA_MAX_LEN : len;
	 #if 0
	 	if (len > RBOOT_OTA_MAX_LEN)
	 	{
	 	  ets_printf("RBOOT OTA: ****************************************\r\n");
	 	  ets_printf("RBOOT OTA: ****************************************\r\n");
	 	  ets_printf("RBOOT OTA: ****************************************\r\n");
	 	  ets_printf("RBOOT OTA: ****************************************\r\n");
	}

#endif
 	//ets_printf("RBOOT OTA: Malloc: %d\r\n", buf_len+status->extra_count);
 	buffer = (uint8 *)os_malloc(buf_len + status->extra_count);
 	if (buffer == NULL)
 	{
 		ets_printf("RBOOT OTA: Malloc failed\r\n");
 		ret = false;
 		goto EXIT_LABEL;
 	}

 	rlen = len;
 	ldata = data;
 	while (rlen > 0)
    {
 	  // Only do a max of 1460 at a time
 	  llen = rlen;
 	  if (rlen > RBOOT_OTA_MAX_LEN)
 	  {
     llen = RBOOT_OTA_MAX_LEN;
 	  }
 	  //ets_printf("RBOOT OTA: rlen: %d llen: %d\r\n", rlen, llen);

     // copy in any remaining bytes from last chunk
     memcpy(buffer, status->extra_bytes, status->extra_count);
     // copy in new data
     memcpy(buffer + status->extra_count, ldata, llen);

     // calculate length, must be multiple of 4
     // save any remaining bytes for next go
     llen += status->extra_count;
     rlen += status->extra_count;
     status->extra_count = llen % 4;
     llen -= status->extra_count;
     rlen -= status->extra_count;
     memcpy(status->extra_bytes, buffer + llen, status->extra_count);

     // check data will fit
     //if (status->start_addr + len < (status->start_sector + status->max_sector_count) * SECTOR_SIZE) {

     // Fixed up code which didn't handle len being greater than SECTOR_SIZE - although
     // this is somewhat moot at sticking to 1460 limit!
     while (status->last_sector_erased < ((status->start_addr + llen) / SECTOR_SIZE))
     {
       status->last_sector_erased++;
       //ets_printf("RBOOT OTA: Erase sector 0x%x\r\n", status->last_sector_erased);
       spi_flash_erase_sector(status->last_sector_erased);
     }

   #if 0
       if (len > SECTOR_SIZE) {
         // to support larger writes we would need to erase current
         // (if not already done), next and possibly later sectors too
       } else {
         // check if the sector the write finishes in has been erased yet,
         // this is fine as long as data len < sector size
         if (status->last_sector_erased != (status->start_addr + len) / SECTOR_SIZE) {
           status->last_sector_erased = (status->start_addr + len) / SECTOR_SIZE;
           spi_flash_erase_sector(status->last_sector_erased);
           ets_printf("RBOOT_OTA: Erase sector 0x%x\r\n", status->last_sector_erased);
         }
       }
   #endif

       // write current chunk
       //ets_printf("RBOOT_OTA: write addr: 0x%08x, len: %d\r\n", status->start_addr, len);
       if (spi_flash_write(status->start_addr, (uint32 *)buffer, llen) == SPI_FLASH_RESULT_OK) {
         status->start_addr += llen;
       }
       else
       {
         ret = false;
         goto EXIT_LABEL;
       }
     //}

     rlen -= llen;
     ldata += llen;
 	}

 	ret = true;

 EXIT_LABEL:


		 if (buffer != NULL)
		   {
		 	  os_free(buffer);
		 	}
	return ret;
}

#ifdef BOOT_RTC_ENABLED
bool ICACHE_FLASH_ATTR rboot_get_rtc_data(rboot_rtc_data *rtc) {
	if (system_rtc_mem_read(RBOOT_RTC_ADDR, rtc, sizeof(rboot_rtc_data))) {
		return (rtc->chksum == calc_chksum((uint8*)rtc, (uint8*)&rtc->chksum));
	}
	return false;
}

bool ICACHE_FLASH_ATTR rboot_set_rtc_data(rboot_rtc_data *rtc) {
	// calculate checksum
	rtc->chksum = calc_chksum((uint8*)rtc, (uint8*)&rtc->chksum);
	return system_rtc_mem_write(RBOOT_RTC_ADDR, rtc, sizeof(rboot_rtc_data));
}

bool ICACHE_FLASH_ATTR rboot_set_temp_rom(uint8 rom) {
	rboot_rtc_data rtc;
	// invalid data in rtc?
	if (!rboot_get_rtc_data(&rtc)) {
		// set basics
		rtc.magic = RBOOT_RTC_MAGIC;
		rtc.last_mode = MODE_STANDARD;
		rtc.last_rom = 0;
	}
	// set next boot to temp mode with specified rom
	rtc.next_mode = MODE_TEMP_ROM;
	rtc.temp_rom = rom;

	return rboot_set_rtc_data(&rtc);
}

bool ICACHE_FLASH_ATTR rboot_get_last_boot_rom(uint8 *rom) {
	rboot_rtc_data rtc;
	if (rboot_get_rtc_data(&rtc)) {
		*rom = rtc.last_rom;
		return true;
	}
	return false;
}

bool ICACHE_FLASH_ATTR rboot_get_last_boot_mode(uint8 *mode) {
	rboot_rtc_data rtc;
	if (rboot_get_rtc_data(&rtc)) {
		*mode = rtc.last_mode;
		return true;
	}
	return false;
}
#endif

#ifdef __cplusplus
}
#endif
